# This Dockerfile creates an image based on archlinux/base that
# runs a zsh shell with the dotfiles deployed and ready.
FROM archlinux/base
ARG user=eikaas

RUN pacman --noconfirm -Syu && pacman --noconfirm -S \
	neovim \
	zsh \
	git \
	tree \
	stow \
	sudo \
	exa \
	gawk \
	z \
	procps \
	neofetch \
	man

RUN ln -s /usr/sbin/nvim /usr/sbin/vim

RUN useradd -ms /usr/bin/zsh $user
RUN gpasswd -a $user wheel
RUN echo "%wheel ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

ADD . /home/$user/.dotfiles
WORKDIR /home/$user/.dotfiles
RUN chown -R $user:$user /home/$user
RUN rm -rf /home/$user/.bashrc

USER $user
RUN stow -vS base

# Fetch git submodules
RUN git submodule init && git submodule update --init --recursive

# Run vim-plug's PlugInstall to fetch all the vim plugins
RUN vim -c "PlugInstall" -c "exit" -c "exit"

ENV XDG_CONFIG_HOME /home/$user/.config
ENV XDG_DATA_HOME /home/$user/.local/share
ENV ZDATADIR /home/$user/.config/zsh
ENV ZDOTDIR /home/$user/.config/zsh

WORKDIR /home/$user
ENTRYPOINT ["/usr/bin/zsh"]
