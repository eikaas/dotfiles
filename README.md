# ~/.*

* [Neovim](https://github.com/neovim/neovim)
* [GNU Stow](https://www.gnu.org/software/stow/)

## Docker
The included Dockerfile creates a container based on the archlinux/base image
with the dotfiles deployed to a normal user account. Use sudo to gain root-privileges.

The username can be changed by adding *--build-var user=yourname* to the docker build
command:
```bash
docker build --rm --build-arg user=lebanana -t dockfiles .
```

## Deployment
Clone the repository and use stow to install the config files to your homedir.
```bash
git clone https://git@github.com/eikaas/dotfiles ~/.dotfiles
cd ~/.dotfiles
git submodules init
git submodule update --init --recursive
stow -vS base
# Now just fix the broken stuff
```

