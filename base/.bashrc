# .bashrc

if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi


# Append to histfile
shopt -s histappend
shopt -s checkwinsize

# Turn off the annoying systemctl/journalctl pager
export SYSTEMD_PAGER=

export HISTFILE="${XDG_DATA_HOME}/.bash_history"

# Display entire hostname in prompt
export PS1="[\u@\H \W]$ "

[[ -f ~/.fzf.bash ]] && source ~/.fzf.bash
