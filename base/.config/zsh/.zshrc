# vim: ft=zsh
export _Z_DATA="${ZDATADIR}/.z"

# Settings
autoload -U compinit && compinit
autoload -U promptinit && promptinit
autoload -U bashcompinit && bashcompinit
autoload -U zsh-mime-setup && zsh-mime-setup
autoload -U zcalc

setopt VI					# Eanble vi mode
setopt COMPLETE_ALIASES				# Autocomplete commandline switches for aliases

setopt MULTIOS					# Pipe to multiple outputs
setopt AUTO_NAME_DIRS				# ???
setopt GLOB_COMPLETE				# Expandglob
setopt NUMERIC_GLOB_SORT
setopt NO_CASE_GLOB				# Case insensitive globbing? 
setopt EXTENDED_GLOB				# Extended globbing
setopt ZLE		
 
# History Settings:
export HISTFILE="${ZDATADIR}/.zsh_history"
setopt APPEND_HISTORY				# Don't overwrite, append!
setopt INC_APPEND_HISTORY			# Write after each command
setopt HIST_IGNORE_DUPS				# If I type cd and then cd again, only save the last one
setopt HIST_IGNORE_ALL_DUPS			# Ignore dupes even if spaced apart a few lines
setopt HIST_REDUCE_BLANKS			# Reduces blanks. No blank things
setopt HIST_IGNORE_SPACE			# Ignore lines starting with space
setopt EXTENDED_HISTORY				# Save Extended history info
setopt HIST_SAVE_NO_DUPS			# Don't save duplicate history entries
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_FIND_NO_DUPS

# Keybindings
bindkey -v
bindkey '^e' end-of-line
bindkey '^a' beginning-of-line
bindkey '^r' history-incremental-search-backward
bindkey -M vicmd "/" history-incremental-search-backward
bindkey -M vicmd "?" history-incremental-search-forward
bindkey -M vicmd "//" history-beginning-search-backward
bindkey -M vicmd "??" history-beginning-search-forward
bindkey -M vicmd "q" push-line
bindkey -M viins 'jj' vi-cmd-mode
bindkey -M vicmd 'u' undo
bindkey -s "\el" " 2>&1|less^M"

# Load dircolors for ls, tree, etc.
which dircolors &>/dev/null && eval $(dircolors -b "${HOME}/.config/lscolors/LS_COLORS")

# Load the currently active base16 shell color theme
BASE16_SHELL="$HOME/.config/base16-shell/"
[[ -n "$PS1" ]] && [[ -s "$BASE16_SHELL/profile_helper.sh" ]] && eval "$($BASE16_SHELL/profile_helper.sh)"

# Use GPG SSH Agent if this silly file exists, usually nope.
if [[ -e $HOME/.gnupg/.use_gpg_ssh_agent ]]; then
  if pid=$(pgrep ssh-agent); then
	  kill -9 ${pid}
  fi

  gpg-connect-agent /bye
  export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
fi

# Commandline Completion:
zmodload -i zsh/complist
zstyle ':completion:*' rehash true
zstyle ':completion:*' users root

[[ -f ~/.local/share/hosts.list ]] && myhosts=($(<~/.local/share/hosts.list))
zstyle ':completion:*:*:*' hosts $myhosts

zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ${ZDATADIR}/cache/

zstyle ':completion:*:*:*:*:processes' menu yes select
#zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'
zstyle ':completion:*:*:*:*:processes' force-list always

# fzf via Homebrew
if [ -e /usr/local/opt/fzf/shell/completion.zsh ]; then
  source /usr/local/opt/fzf/shell/key-bindings.zsh
  source /usr/local/opt/fzf/shell/completion.zsh
fi
# fzf via local installation
if [ -e ~/.fzf ]; then
  PATH="$PATH:~/.fzf/bin"
  source ~/.fzf/shell/key-bindings.zsh
  source ~/.fzf/shell/completion.zsh
fi

# fzf + ag configuration
if which fzf &>/dev/null && which ag &>/dev/null; then
  export FZF_DEFAULT_COMMAND='ag -g ""'
  export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
  export FZF_ALT_C_COMMAND="$FZF_DEFAULT_COMMAND"
  export FZF_DEFAULT_OPTS='
  --height 60% --border --inline-info
  --color fg:33,bg:0,hl:42,fg+:15,bg+:236,hl+:108
  --color info:108,prompt:109,spinner:108,pointer:168,marker:168
  '
fi
export FZF_COMPLETION_TRIGGER='zz'
[[ -f ~/.fzf.zsh ]] && source ~/.fzf.zsh

# Oh My ZSH
export ZSH="${ZDOTDIR}/oh-my-zsh"
HYPHEN_INSENSITIVE="true"
DISABLE_AUTO_UPDATE="true"
# ZSH_THEME="gallifrey"
# ZSH_THEME="half-life"
# ZSH_THEME="random"
# ZSH_THEME="kennethreitz" (Nice, but missing hostname)
ZSH_THEME="af-magic"
plugins=(git z vi-mode fzf zsh-syntax-highlighting)
source "${ZSH}/oh-my-zsh.sh"

# Aliases
source "${ZDOTDIR}/alias.zsh"

# Gopass completion
autoload -U compinit && compinit
autoload -U bashcompinit && bashcompinit

if hash gopass; then
  # Produces 4-5 lines of errors/warnings that I havent looked into yet
  source <(gopass completion zsh) 2>/dev/null 
fi

if hash kubectl; then
  source <(kubectl completion zsh)
fi

