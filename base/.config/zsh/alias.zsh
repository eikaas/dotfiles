# ~/.zsh/alias.zsh

# Default overrides
alias cp='cp -v'
alias mv='mv -v'
alias rm='rm -v'
alias figlet='figlet -w 300 -f univers'

alias genpw="base64 <(cat /dev/urandom | head -c 12)"

alias ls="exa --group-directories-first --git --color-scale -mghrs size"
alias lst="exa --group-directories-first --git --tree --color-scale -mghrs size"
alias lsg="exa --group-directories-first --long --sort name -G"
alias ls="exa --group-directories-first --git --color-scale -mr"

alias le="exa -lrhgHBimU --git --group-directories-first"

alias encrypt='gpg -ear re@webhuset.no'
alias decrypt='gpg --decrypt'

# Utilities
alias hx='hexdump -Cv'
alias duh='du -ah --max-depth=1'
alias whkeys="echo -n 'bash <(curl -sL --fail https://whbox.webhuset.no/whkeys.sh)' | pbcopy"
alias cpkey="echo -n 'install -m 700 -d ~/.ssh && curl --fail -sL https://www.webhuset.no/keys/ssh/id_rsa.re.webhuset.no.pub >> ~/.ssh/authorized_keys && chmod 600 ~/.ssh/authorized_keys' | pbcopy"
alias cpallkeys="echo -n 'for nn in re sm lh mb hv; do install -m 700 -d ~/.ssh && curl --fail -sL https://www.webhuset.no/keys/ssh/id_rsa.${nn}.webhuset.no.pub >> ~/.ssh/authorized_keys && chmod 600 ~/.ssh/authorized_keys; done' | pbcopy"
alias dstamp="echo -n $(date +%m-%d-%y_%H.%M.%S) | pbcopy"
alias plotline="gnuplot -p -e 'plot \"/dev/stdin\"'"
alias grepnum="grep -Po '\d+(\.\d+)?'"
alias lsummer='awk "BEING{s=0}{s+=$1}END{print s}"'
alias ymlchk="python -c 'import yaml,sys;yaml.safe_load(sys.stdin)' 2>/dev/null < $1"
alias rapetun='for pid in $(ps aux | grep Tunnelblick | awk '{print $2}' | grep -v grep); do kill $pid; done'

# Git
alias ga='git add'
alias gitpush='git push origin master'
alias gitpull='git pull'
alias gd='git diff --color --no-ext-diff'
alias gdstat='git diff --color --stat'
alias gs='git status -sb'
alias glog="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias gitbranches="git for-each-ref --sort='-authordate' --format='%(authordate)%09%(objectname:short)%09%(refname)' refs/heads | sed -e 's-refs/heads/--'"
alias gcpush='git commit -a -m && git push origin'
alias gundo='git reset --soft HEAD~1'
alias gupdatesubm='pull --recurse-submodules && git submodule update --recursive --remote'

# Docker
alias drun='docker run --rm -t'

# assh wraps ssh and provides much more flexible
# yaml based configuration. If assh is installed make ssh an alias to it
if hash assh; then
  alias ssh="assh wrapper ssh"
fi

function sortlen() {
  awk '{ print length, $0 }' | sort -n -s -r | cut -d" " -f2-
}

function markd() {
  pandoc $1 | lynx -stdin
}
