export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"

if [[ "$(uname)" == "Darwin" ]]; then
	export PATH="/usr/local/opt/coreutils/libexec/gnubin:/usr/local/bin:/usr/local/sbin:$PATH"
	export MANPATH="/opt/local/share/man:/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"
fi

export EDITOR="vim"
export PAGER="less"
export LC_ALL="en_US.UTF-8"
export LANG="en_US.UTF-8"
export LANGUAGE="en_US.UTF-8"
export TZ="Europe/Oslo"

export SAVEHIST=3500
export HISTSIZE=3500
export HISTFILESIZE=3500
export HISTCONTROL=ignoredups:erasedups
export HISTIGNORE="ls:ll:cd:pwd"
export HISTTIMEFORMAT="[$(tput setaf 6)%F %T$(tput sgr0)]: "

# Supposedly we don't need to set this anymore, but leaving it out is messing up gometalinter
export GOROOT="/usr/local/go"
export GOPATH="$HOME/go"

export PATH="$PATH:/usr/local/go/bin:/usr/local/bin:$HOME/bin:$GOPATH/bin:$HOME/.cargo/bin"

test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"


export PATH="$HOME/.cargo/bin:$PATH"
