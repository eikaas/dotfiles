# vim: ft=zsh
# Source .profile for general env
[[ -e ~/.profile ]] && emulate sh -c 'source ~/.profile'

# Overrides where ZSH looks for its config files
export ZDOTDIR="${XDG_CONFIG_HOME}/zsh"

# ZSH doesnt actually use this
export ZDATADIR="${XDG_DATA_HOME}/zsh"
